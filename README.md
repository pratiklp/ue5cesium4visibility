# Development of low visibility conditions for automotive and aerospace research
Developer: Pratik Pradhan (https://www.linkedin.com/in/pradhanpratik/)

<img src="splash_RAW.png"
        alt="Open World Map Setup on Unreal Engine"
        style="float: center;"
        width="100%">

## Getting started

To download, create a clone of the project. 

```
git clone https://gitlab.com/pratiklp/ue5cesium4visibility.git
```

Once cloned, open the Unreal Engine 5.3 project titled `WeatherCSM.uproject` inside the `WeatherCSM` folder. 

## Goal
The purpose of this project is to develop realistic low visibility conditions possible through various factors in real life including the time of day, overcast clouds, rain, fog, etc. that can be used in studies on aviation and automotive navigation research applications.

## About the project
For any pilot of an aircraft or a car driver, one of the biggest hurdles is driving without a clue where s/he is navigating. On a rainy or snowy day, it is difficult for a ground vehicle driver to identify lanes correctly or determine other passing cars in their blind spot regions. On a foggy day, they might not even know what is right in front of them. I had driven through the Gardiner Expressway a couple of times when it was so foggy that I thought brake lights mounted on top of a trailer seemed like road lamps. It had to be my intuition alone that I knew it was a trailer, and I needed to keep at least 3 cars distance from the source of that light. I am a researcher of human-machine interface and human factors in aviation, dealing with the usability of Extended Reality (XR) technology to enhance the pilot experience in a simulated environment. For my research, I have noticed that one of the biggest concerns for pilots who fly an aircraft is to fly in situations where they cannot visually determine what is in front of them. When taxiing the aircraft from the ramp to the runway, they must maintain their position along the taxiway centerlines. Likewise, they need to see the runway and the flight instruments' indications during landing. In human factors research, it is crucial that such situations can be mimicked as close to reality as possible. Unreal Engine is known to have the ability to generate photorealism better than any other 3D development platform widely used in the 3D development and gaming industry. Unreal Engine with Cesium provides the most straightforward and fastest solution to creating a close replica of the planet Earth, including 3D models of the buildings in most regions. With a reliable visibility replication tool, Cesium’s Unreal Engine plugin would be one step closer to a flight simulator capable of many scientific studies and comparable to existing flight simulators such as X-Plane 12, Microsoft Flight Simulator, etc.

## Design
For this project, I am trying to create a set of assets that could be plugged into any open world map built with Cesium Georeference that can be used to mimic the natural weather using the resources available via Cesium and Unreal Engine 5.3. For the sample map, I set up the georeferenced origin at the Tenzing-Hillary Airport (aka Lukla airport), a small domestic airport in Nepal. In it, I added a modified version of SunSky class, and a Weather class, as illustrated in Figure 1.

<p>
    <img src="Architecture Diagrams/WeatherCSM-Page-1.jpg"
        alt="Open World Map Setup on Unreal Engine"
        style="float: center;"
        width="100%"><br>
    <em>Figure 1: Open World Map Setup on Unreal Engine</em>
</p>

### Real-time Day/Time Class
For this class, I used the built-in UE5 SunSky instead of the Cesium SunSky due to easier customization through Blueprint. The modified SunSky class provides an option to access real date and time and uses geodetic coordinates directly from Cesium Georeference input. Should the developer prefer to use user-defined data, they can turn off the real-time feature and use custom data for date and time. In the current version, developers would be required to manually enter the time zone on the class actor’s details panel, but in the future, a predetermined library of latitude and corresponding time zone would be used to completely automate the system. To help the SunSky feature work as efficiently as Cesium SunSky with the sample map, the modified class also uses identical parameters for Sky Light, Directional Light, and Sky Atmosphere components. The class also contains Cesium Globe Anchor to link the actor with Cesium Georeference in the map directly. Finally, the class also contains post-processing component which is used to enhance the overall lighting of the environment without having to introduce any new setup on the map separately. Following the instructions provided in Cesium website’s guidelines for lighting setup using UE5’s post-processing feature, the post processing component in this class contains various modifications in parameters including bloom intensity, minimum exposure, lens flare intensity, image effects, and color grading. The class also uses a post-processing material under the rendering features using setup instructions for remote rendering through Magic Leap 2 mixed reality headset. 

<p>
    <img src="Architecture Diagrams/WeatherCSM-Page-3.jpg"
        alt="Solar Time Setup using modified UE5 SunSky that uses real-time location and date/time for output"
        style="float: center;"
        width="100%"><br>
    <em>Figure 2: Solar Time Setup using modified UE5 SunSky that uses real-time location and date/time for output</em>
</p>

### Weather Class
The Weather class is the heart of the project and is mostly under works at the moment. The idea for this class is to provide the developers with options to introduce customizable options for different types of visibility scenarios. Currently, the class is built up of a volumetric cloud, an atmospheric fog, an exponential height fog, and a rain effect. As shown in Figure 3, developers can choose to make it rain in the surrounding. For now, the map has this weather class set as a child of the Dynamic Pawn allowing the environmental factor to be impacted wherever the dynamic pawn is headed during runtime. When the event starts playing, if the rain is turned on, a fixed value of local height fog is introduced. The local height fog is a new feature introduced in Unreal Engine 5 which allows developers to add fog in a local space for lighter development purposes. However, its blueprint class currently lacks an option to change the intensity value through blueprint scripting. So, a fixed value of 0.25 has been set as it provides somewhat realistic visibility during heavy rain architecture. In the future, this fixed value will be changed to a variable option for better efficiency. In every frame, the class checks whether rain, snow or dust are turned on. That said, currently, only rain FX has been programmed. 

<p>
    <img src="Architecture Diagrams/WeatherCSM-Page-4.jpg"
        alt="Weather class setup"
        style="float: center;"
        width="100%"><br>
    <em>Figure 3: Weather class setup</em>
</p>

If the rain is turned on, the Rain FX (Figure 4) is activated. To limit memory usage, the Weather class would have to be placed as a child object of the Dynamic Pawn. However, alternative and more efficient methods will be explored in the future. 

<p>
    <img src="Architecture Diagrams/RainFX.png"
        alt="Rain FX Setup"
        style="float: center;"
        width="100%"><br>
    <em>Figure 4: Rain FX Setup</em>
</p>

For the Rain FX (Figure 4), I used a set of emitters each for different purposes as instructed in references [1] and [2]. They include: -

1)	Rain
This emitter lets developers choose the origin, spawn rate, and color of the water drops. It is situated within a box space with a fixed bound of 900 cm from the local origin on all three axes. It allows developers to enter the spawn rate and color of the water droplets. It also generates a collision event which allows other emitters to access to activate. 

<p>
    <img src="Architecture Diagrams/RainEmitter.png"
        alt="Rain emitter through Niagara FX"
        style="float: center;"
        width="100%"><br>
    <em>Figure 5: Rain emitter through Niagara FX</em>
</p>

2)	Rain puddle
Upon receiving the collision event, this emitter generates small puddles upon impact with the ground, in a torus shaped construction.

<p>
    <img src="Architecture Diagrams/PuddleMesh.png"
        alt="Puddle Mesh FX setup"
        style="float: center;"
        width="100%"><br>
    <em>Figure 6: Puddle Mesh FX setup</em>
</p>

3)	Water splash
This emitter is used to create splash effects. When the water hits ground, it generates the puddle as shown in Figure 6, and creates a splash effect to mimic water bouncing off the ground. Currently, it does not appear great on a Cesium map, but I am working on improving it as it could help create some additional contributions to large simulations visibility tests.

<p>
    <img src="Architecture Diagrams/WaterSplash.png"
        alt="Water splash effect using Niagara FX"
        style="float: center;"
        width="100%"><br>
    <em>Figure 7: Water splash effect using Niagara FX</em>
</p>

4)	Water dips
Once the water hits the ground, it generates a wave inward around the puddle generated. Since the puddle and splashes are not aligned perfectly to the ground yet, the water dips are almost untestable at the moment. But, works are being done to incorporate it into the rain simulation. 

<p>
    <img src="Architecture Diagrams/WaterDipsMaterial.png"
        alt="Material created for water dips effect"
        style="float: center;"
        width="100%"><br>
    <em>Figure 8: Material created for water dips effect</em>
</p>

5)	Rain puddle decals
Finally, once the water hits the ground and generates the puddle, a tiny mist-like effect is expected around the collided surface. I have not been able to implement this as intended either, but all the components have been set up as intended. Perfecting it could add to the degradation of visibility which could help with many lane tracking applications.

<p>
    <img src="Architecture Diagrams/PuddleMesh.png"
        alt="Puddle decals setup"
        style="float: center;"
        width="100%"><br>
    <em>Figure 9: Puddle decals setup</em>
</p>

## Runtime Inputs
The map contains a Helicopter simulation which allows a user to take-off from Runway 24 with or without the rain turned on. To control the flight, you can use the following controls:-
|   |   Command |   Keyboard    |   XBox Controller |
|---|-----------|---------------|-------------------|
| 1 |Increase RPM/Take-off/Ascend   |   Q   |   LT  |
| 2 |Decrease RPM/Descend/Land      |   E   |   RT  |
| 3 |Fly (Pitch, Roll)      |   W, A, S, D   |  LS  |
| 4 |Fly (Yaw)              |   Left, Right  |  B, X    |
| 5 |Look around            |   Mouse (Hover)   |   RS  |
| 6 |Switch Camera          |   Tab | Special Left      |

The helicopter blueprint was obtained from https://www.unrealengine.com/marketplace/en-US/product/helicopter-template-01?sessionInvalidated=true and uses similar or same input controllers, with minor modifications made in camera handling. 

## Results
The goal of this project was to create an asset that would allow developers and engineers to test various visibility conditions on Cesium’s geospatial map using Unreal Engine 5.3. To do so, the Cesium Georeference origin was set up at the Tenzing-Hillary Airport (also known as Lukla Airport) (Figure 10), in Nepal. Given it is the most dangerous airport in the world, has one of the most challenging low-visibility situations, and is situated in the highest altitude making it a perfect test for a wide range of visibility testing conditions, the airport runway has been selected for initial tests. At the moment, two blueprint classes handle the visibility situations in the map. The first is the Real-time Day/Time class, and the second is the Weather class. The Real-time Day/Time class handles the natural lighting conditions based on either the current date, time and georeferenced position of the map, or allows developers to enter a given date, or time. Likewise, the Weather class is responsible for generating various weather-related low-visibility contributors such as cloud, fog, rain, snow, dust, etc. So far, the class only simulates cloud (using UE5’s built-in Volumetric Cloud component), atmospheric fog, exponential height fog, local height fog, and rain.

<p>
    <img src="Architecture Diagrams/lukla_airport_clear.png"
        alt="Tenzing-Hillary Airport: Day with mild fog and no rain"
        style="float: center;"
        width="100%"><br>
    <em>Figure 10: Tenzing-Hillary Airport: Day with mild fog and no rain</em>
</p>

<p>
    <img src="Architecture Diagrams/lukla_low_vis_heavyFog.png"
        alt="Tenzing-Hillary Airport – Day with heavy fog"
        style="float: center;"
        width="100%"><br>
    <em>Figure 11: Tenzing-Hillary Airport – Day with heavy fog</em>
</p>

<p>
    <img src="Architecture Diagrams/lukla_rain.png"
        alt="Tenzing-Hillary airport with Rain effect using Niagara FX (editor)"
        style="float: center;"
        width="100%"><br>
    <em>Figure 12: Tenzing-Hillary airport with Rain effect using Niagara FX (editor)</em>
</p>

To test the efficiency, I measured the frame rate under various conditions while flying around using the dynamic pawn.  Results are presented in Table 1.

|   | Conditions | Visibility | Minimum Frame Rate | Maximum Frame Rate |
|---|------------|------------|--------------------|--------------------|
|1  |Clear conditions with no fogs, clouds, and not in real-time|Very low at night; Very high at day|95|120|
|2	|Clear conditions with no fogs, clouds, and in real-time|No change|95|120|
|3	|Clear conditions with volumetric clouds|No change|95|120|
|4	|Foggy conditions with cloud, exponential height fog|Low beyond the runway|86|120|
|5	|Foggy conditions with cloud, exponential height fog, local height fog|Low in the runway and very low beyond the runway|86|120|
|6	|Foggy conditions with cloud, exponential height, atmospheric fog|Low beyond the runway|86|120|
|7	|Rainy condition with cloud, fog (Spawn rate = 100)|Low in the runway and very low beyond the runway|75|120|
|8	|Rainy condition with cloud, fog (Spawn rate = 1000)|Same as 7; Rain is more visible|75|120|
|9	|Rainy condition with cloud, fog (Spawn rate = 10000)|Same as 7; runway visibility is slightly lower|75|120|
|10	|Rainy condition with cloud, fog (Spawn rate = 100000)|Same as 7; not much change with the visibility|75|120|


## Conclusion
With the given time and resources, I was able to create proof of concept. And the more I developed the project, the more I understood the significance of such work in my own or other researchers working toward helping drivers and pilots navigate through degraded visual environments. Simulators such as X-Plane or Microsoft Flight Simulator do have some capacity of environment control operations for such scenarios, but they are not fully controllable or customizable, and cannot be used in UE-based simulators. Hence, I decided to use this project as a stepping stone for my ongoing research on Human-Machine Interface (HMI) for improved pilot interaction for next-generation aviation research. 

All the code for this document can be found at: https://gitlab.com/pratiklp/ue5cesium4visibility.git

For future work, I intend to continue working on improving the rain effect within the next couple of months, followed by snow and dust effects for another 4-6 months. Once all the effects have been implemented, I plan on adding more options for developers to be able to adjust intensity, color and other relevant parameters that could be useful in creating a near-real-life weather-induced weather simulation on Unreal Engine for aerospace and automotive research, including a couple of my own.

## Demo

![Demo Video](DemoRecording.mp4){width=100%}
YouTube Link: https://youtu.be/y5sl32mHHzc
Helicopter Simulation: https://youtu.be/WmPPHA4NdmQ
<!-- 
[![Demo Video](http://img.youtube.com/vi/y5sl32mHHzc/0.jpg)](https://youtu.be/y5sl32mHHzc) -->

## References
1. 	How to create Rain in Unreal Engine 4 Niagara – Kids With Sticks [Internet]. [cited 2024 Feb 29]. Available from: https://kidswithsticks.com/how-to-create-rain-in-unreal-engine-4-niagara/
2. 	Unreal Engine Rain Particle with Splashes using Niagara FX System 🌧 [Internet]. 2021 [cited 2024 Feb 29]. Available from: https://www.youtube.com/watch?v=qmZCW7eQ6rc